from django.shortcuts import render

# Create your views here.
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from users_orders.serializers import CartSerializer, CouponCodeSerializer

from users_orders.models import CouponCode, UserCart

from menusetup.views import ReadOnly


class UserCartView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        queryset = UserCart.objects.all()
        serializer = CartSerializer(queryset, many=True)
        return Response(serializer.data)

    def post(self):
        serializer = CartSerializer(data=self.request.data)
        data = {}
        if serializer.is_valid():
            token = Token.objects.filter(key=self.request.headers['token']).first()
            coupon = CouponCode.objects.filter(coupon_code=serializer.validated_data['coupon_code']).first()
            cart = serializer.objects.user_cart(
                user=token.user,
                coupon_code=coupon,
                address=serializer.validated_data['address'],
                qty=serializer.validated_data['qty'],
                price_per_qty=serializer.validated_data['price_per_qty'],
                coupon_code_discount=serializer.validated_data['coupon_code_discount'],
                from_date_range_subscription=serializer.validated_data['from_date_range_subscription'],
                to_date_range_subscription=serializer.validated_data['to_date_range_subscription'],
            )
            return Response({"message": 'Cart Saved', "code": 200})
        else:
            return Response({"message": 'User not logged in', "code": 600})


class CouponCodeView(APIView):
    permission_classes = (ReadOnly,)

    def get(self, request, **kwargs):
        serializer = CouponCodeSerializer(CouponCode.objects.filter(active=True), many=True)
        return Response(serializer.data)
