from django.conf.urls import url
from django.urls import path
from users_orders.views import UserCartView, CouponCodeView


urlpatterns = [
    # ...

    # Some where in your existing urlpatterns list, Add this line
    path('', UserCartView.as_view(), name="user-cart"),
    path('coupon-code-list', CouponCodeView.as_view(), name="user-cart"),
    path('coupon-code-list', CouponCodeView.as_view(), name="user-cart"),
    # ...
]