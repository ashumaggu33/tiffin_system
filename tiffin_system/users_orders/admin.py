from django.contrib import admin

# Register your models here.
from .models import CouponCode


@admin.register(CouponCode)
class MenuInstanceAdmin(admin.ModelAdmin):
    list_filter = ('min_cart_value', 'max_discount')
    list_filter1 = ('valid_from', 'valid_to')

    fieldsets = (
        (None, {
            'fields': ('coupon_code', 'category', 'coupon_descr', 'coupon_code_percent')
        }),
        ('Discount On ', {
            'fields': (list_filter,)
        }),
        ('Validity', {
            'fields': (list_filter1,)
        }),
        ('Availability', {
            'fields': ('active',)
        }),
    )
