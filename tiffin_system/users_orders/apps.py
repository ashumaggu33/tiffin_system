from django.apps import AppConfig


class UsersOrdersConfig(AppConfig):
    name = 'users_orders'
