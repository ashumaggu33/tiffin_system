import django
from django.db import models
# Create your models here.
from django.db.models import DO_NOTHING
from django.utils import timezone
from menusetup.models import Category, Menu
from user_listing.models import User


class CouponCode(models.Model):
    coupon_code = models.CharField(unique=True, max_length=255, verbose_name='Coupon Code')
    coupon_descr = models.TextField(verbose_name="Coupon Description", null=True, blank=True)
    category = models.ForeignKey(Category, related_name='coupon_code', on_delete=DO_NOTHING)
    coupon_code_percent = models.IntegerField(verbose_name='coupon code percent')
    min_cart_value = models.IntegerField(verbose_name='Minimum Cart Amount', null=True, blank=True)
    max_discount = models.IntegerField(verbose_name='Upto Discount', null=True, blank=True)
    valid_from = models.DateTimeField(verbose_name='valid from')
    valid_to = models.DateTimeField(verbose_name='Valid To')
    active = models.BooleanField(default=False)
    created_at = models.DateTimeField(verbose_name='Created at', default=django.utils.timezone.now)

    def __str__(self):
        return self.coupon_code


class Orders(models.Model):
    orderId = models.CharField(unique=True, max_length=255)
    user = models.ForeignKey(User, related_name='orders_user', on_delete=DO_NOTHING)
    coupon_code = models.ForeignKey(CouponCode, related_name='orders_coupon_code', on_delete=DO_NOTHING)
    address = models.CharField(verbose_name='Order Address', max_length=255)
    order_price = models.FloatField(verbose_name='Order Price')
    order_placed = models.DateTimeField(verbose_name='Order Placed At')

    class Meta:
        verbose_name = 'Orders'
        verbose_name_plural = 'Orders'

    def __str__(self):
        return self.orderId


class OrdersData(models.Model):
    ORDERS_CHOICE = (
        ('Cancel', 'Cancel'),
        ('Confirm', 'Confirm'),
        ('In Progress', 'In Progress'),
    )
    menu = models.ForeignKey(Menu, related_name='orders_data_menu', on_delete=DO_NOTHING)
    order = models.ForeignKey(Orders, related_name='orders_data', on_delete=DO_NOTHING)
    qty = models.IntegerField()
    price_per_qty = models.FloatField(verbose_name='Price per Quantity(incl. VAT)')
    coupon_code_discount = models.FloatField(verbose_name='Coupon Code Discount')
    from_date_range_subscription = models.DateField(verbose_name='From date')
    to_date_range_subscription = models.DateField(verbose_name='To date')
    order_status = models.CharField(choices=ORDERS_CHOICE, max_length=255)

    def __str__(self):
        return self.price_per_qty

    class Meta:
        verbose_name = 'Order Details'
        verbose_name_plural = 'Orders Details'


class UserCart(models.Model):
    user = models.ForeignKey(User, related_name='cart_user', on_delete=DO_NOTHING)
    coupon_code = models.ForeignKey(CouponCode, related_name='cart_coupon_code', on_delete=DO_NOTHING)
    address = models.CharField(verbose_name='Cart Address', max_length=255)
    qty = models.IntegerField()
    price_per_qty = models.FloatField(verbose_name='Price per Quantity(incl. VAT)')
    coupon_code_discount = models.FloatField(verbose_name='Coupon Code Discount')
    from_date_range_subscription = models.DateField(verbose_name='From date')
    to_date_range_subscription = models.DateField(verbose_name='To date')

    class Meta:
        verbose_name = 'User Cart'
        verbose_name_plural = 'User Cart'

    def __str__(self):
        return self.user
