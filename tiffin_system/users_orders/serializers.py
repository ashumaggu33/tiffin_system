from rest_framework.serializers import ModelSerializer

from users_orders.models import UserCart, CouponCode


class CartSerializer(ModelSerializer):

    class Meta:
        model = UserCart
        fields = '__all__'

    def create(self, validated_data):
        cart = super(CartSerializer, self).create(validated_data)
        cart.save()
        return cart

    def user_cart(self, **kwargs):
        usercart = UserCart()
        usercart.user = kwargs.get('user')
        usercart.coupon_code = kwargs.get('coupon_code')
        usercart.address = kwargs.get('address')
        usercart.coupon_code_discount = kwargs.get('coupon_code_discount')
        usercart.from_date_range_subscription = kwargs.get('from_date_range_subscription')
        usercart.to_date_range_subscription = kwargs.get('to_date_range_subscription')
        usercart.price_per_qty = kwargs.get('price_per_qty')
        usercart.qty = kwargs.get('qty')
        usercart.save()
        return usercart


class CouponCodeSerializer(ModelSerializer):

    class Meta:
        model = CouponCode
        fields = '__all__'