import os

from django.conf import settings
from django.db import models

# Create your models here.
from django.db.models import DO_NOTHING
from django.utils.safestring import mark_safe


class Category(models.Model):

    category_name = models.CharField(unique=True, max_length=255)
    category_desc = models.CharField(max_length=255)
    category_img = models.ImageField(upload_to='category_images/')
    status = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def image_tag(self):
        return mark_safe('<img src="/media/%s" width="150" height="150" />' % str(self.category_img))
    image_tag.short_description = 'Image'
    image_tag.allow_tags = True

    def image_url(self):
        return self.category_img.url

    def __str__(self):
        return self.category_name


class Menu(models.Model):

    item_name = models.CharField(unique=True, max_length=255, verbose_name='Item Name')
    description = models.TextField(verbose_name='Item Description')
    category = models.ForeignKey(Category, related_name='menu', on_delete=DO_NOTHING)
    menu_img = models.ImageField(upload_to='menu_images/')
    start_date = models.DateField(verbose_name='Start Date')
    end_date = models.DateField(verbose_name='End Date')
    rank = models.IntegerField()
    is_active = models.BooleanField(default=False, verbose_name='Status')

    class Meta:
        verbose_name = 'Menu'
        verbose_name_plural = 'Menu for Category'

    def menu_image_tag(self):
        return mark_safe('<img src="%s" width="150" height="150" />' % str(self.menu_img.url))
    menu_image_tag.short_description = 'Image'
    menu_image_tag.allow_tags = True

    def menu_image_url(self):
        return self.menu_img.url

    def __str__(self):
        return self.item_name


class CategoryDurationPackage(models.Model):

    days = models.IntegerField()
    amount = models.IntegerField()
    category = models.ForeignKey(Category, related_name='duration', on_delete=DO_NOTHING)

    class Meta:
        verbose_name = 'Category Duration Package'
        verbose_name_plural = 'Category Duration Package'

    def __str__(self):
        return str(self.days)


class MenuDurationPackage(models.Model):

    days = models.IntegerField(null=True, blank=True)
    amount = models.IntegerField(null=True, blank=True)
    menu = models.ForeignKey(Menu, related_name='menu_duration', on_delete=DO_NOTHING)

    class Meta:
        verbose_name = 'Menu Duration Package'
        verbose_name_plural = 'Menu Duration Package'

    def __str__(self):
        return str(self.days)
