from django.contrib import admin

# Register your models here.
from rest_framework.authtoken.models import Token

from .models import Category, Menu, CategoryDurationPackage, MenuDurationPackage
from django.contrib import admin
from django.contrib.auth.models import Group

admin.site.unregister(Group)
admin.site.unregister(Token)


class CategoryDurationPackageInline(admin.TabularInline):
    model = CategoryDurationPackage


class MenuDurationPackageInline(admin.TabularInline):
    model = MenuDurationPackage


@admin.register(Category)
class CategoryInstanceAdmin(admin.ModelAdmin):
    # list_filter = ('status', 'due_back')

    fieldsets = (
        (None, {
            'fields': ('category_name', 'category_img')
        }),
        ('Image', {
            'fields': ('image_tag',)
        }),
        ('Availability', {
            'fields': ('status',)
        }),
    )
    # fields = ['image_tag']
    readonly_fields = ['image_tag']
    inlines = [CategoryDurationPackageInline]


@admin.register(Menu)
class MenuInstanceAdmin(admin.ModelAdmin):
    list_filter = ('start_date', 'end_date')

    fieldsets = (
        (None, {
            'fields': ('category', 'item_name', 'description', list_filter)
        }),
        ('Image', {
            'fields': ('menu_img', 'menu_image_tag',)
        }),
        ('Availability', {
            'fields': ('is_active',)
        }),
    )
    readonly_fields = ['menu_image_tag']
    inlines = [MenuDurationPackageInline]
