from rest_framework import serializers
from .models import Category, CategoryDurationPackage, Menu, MenuDurationPackage


class CategoryDurationSerializer(serializers.ModelSerializer):
    plan_id = serializers.CharField(source='id')

    class Meta:
        model = CategoryDurationPackage
        fields = ['plan_id', 'days', 'amount']


class MenuDurationSerializer(serializers.ModelSerializer):

    class Meta:
        model = MenuDurationPackage
        fields = ['id', 'days', 'amount']


class CategorySerializer(serializers.ModelSerializer):
    category_id = serializers.CharField(source='id')
    duration = CategoryDurationSerializer(many=True, read_only=True)
    category_image_url = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = ['category_id', 'category_name', 'status', 'duration', 'category_image_url']

    def get_category_image_url(self, category):
        request = self.context.get('request')
        return request.build_absolute_uri(category.category_img.url)


class MenuSerializer(serializers.ModelSerializer):
    # menu = serializers.StringRelatedField(many=True)
    package_details = MenuDurationSerializer(many=True, read_only=True)
    menu_image = serializers.SerializerMethodField()

    class Meta:
        model = Menu
        fields = ['id', 'item_name', 'description', 'is_active', 'menu_image', 'package_details',
                  'start_date', 'end_date']

    def get_menu_image(self, menu):
        request = self.context.get('request')
        return request.build_absolute_uri(menu.menu_img.url)


class CategoryMenuSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)
    menu = MenuSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ['category', 'menu']
