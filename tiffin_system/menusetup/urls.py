from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path
from django.views.generic import TemplateView
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view
from menusetup.views import CategoryViewSet, MenuViewSet, PlanSelectedViewSet
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

router = routers.DefaultRouter()

schema_view = get_schema_view(
   openapi.Info(
      title="Apni Rasoi API",
      default_version='v1',
      description="Tiffin and Food Services",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="apnirasoiservices@gmail.com"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    url('docs/$', schema_view),
    url('category-listing/$', CategoryViewSet.as_view()),
    url('menu-listing/(?P<pk>.+)/$', MenuViewSet.as_view()),
    url('plan-select/(?P<plan_id>.+)/$', PlanSelectedViewSet.as_view()),
]
urlpatterns += router.urls
