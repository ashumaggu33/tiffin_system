from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404

# Create your views here.
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets
from django.core import serializers

from menusetup.models import Category, Menu, CategoryDurationPackage
from menusetup.serializers import CategorySerializer, MenuSerializer, CategoryMenuSerializer, CategoryDurationSerializer
from rest_framework.permissions import SAFE_METHODS
from rest_framework.views import APIView, Response


class ReadOnly(object):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS


class CategoryViewSet(APIView):
    permission_classes = [ReadOnly]

    @swagger_auto_schema(responses={200: CategorySerializer(many=True)})
    def get(self, request, **kwargs):
        queryset = Category.objects.all()
        serializer_class = CategorySerializer(queryset, many=True, context={"request": request})
        return JsonResponse(serializer_class.data, safe=False)


class MenuViewSet(APIView):
    permission_classes = [ReadOnly]

    @swagger_auto_schema(responses={200: MenuSerializer(many=True)})
    def get(self, request, **kwargs):
        queryset = Category.objects.filter(pk=kwargs['pk'])
        serializer_class = CategoryMenuSerializer(queryset, many=True, context={"request": request})
        return JsonResponse(serializer_class.data, safe=False)


class PlanSelectedViewSet(APIView):
    permission_classes = [ReadOnly]

    @swagger_auto_schema(responses={200: MenuSerializer(many=True)})
    def get(self, request, **kwargs):
        queryset = CategoryDurationPackage.objects.filter(pk=kwargs['plan_id'])
        serializer_class = CategoryDurationSerializer(queryset, many=True, context={"request": request})
        return JsonResponse(serializer_class.data, safe=False)
