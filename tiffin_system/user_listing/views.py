import json
import random

from django.contrib.auth import authenticate
from django.contrib.auth.hashers import make_password
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.db.models.functions import datetime

# Create your views here.
from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions, status, generics, viewsets
from rest_framework.decorators import api_view
from rest_framework.permissions import AllowAny, IsAuthenticated, SAFE_METHODS
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.status import HTTP_401_UNAUTHORIZED
from rest_framework.views import APIView

from .models import UserManager, OtpToVerify
from .serializers import UserSerializer, ProfileSerializer, UserLoginSerializer, UserProfileSerializer, \
    LogOutSerializer, UserOTPSerializer, SetPasswordSerializer, UserPasswordSerializer, UserAddressSerializer
from django.shortcuts import get_object_or_404
from user_listing.models import User

from tiffin_system.settings import EMAIL_HOST_USER
from rest_framework.authtoken.models import Token


class ReadOnly(object):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS


class ProfileAPI(viewsets.ViewSet):
    permission_classes = [ReadOnly]

    @swagger_auto_schema(responses={200: ProfileSerializer(many=True)})
    def list(self, request):
        queryset = User.objects.all()
        serializer = ProfileSerializer(queryset, many=True)
        return Response(serializer.data)


def create_auth(request):
    serialized = UserSerializer(data=request.data)
    data = {}
    if serialized.is_valid():
        user = User.objects.create_user(
            email=serialized.validated_data['email'],
            date_joined=datetime.datetime.now(),
        )
        if user:
            token = Token.objects.create(user=user)
            json = serialized.data
            json['token'] = token.key
            data['data'] = serialized.data
            data['token'] = token.key
        else:
            return Response({"message": serialized._errors, "token": {}, "code": 600})

        mail_not_sent = ''
        try:
            subject = 'Welcome to Apni Rasoi Services'
            message = 'Hope you are enjoying'
            recepient = str(serialized.validated_data['email'])
            send_mail(subject, message, EMAIL_HOST_USER, [recepient], fail_silently=False)
        except:
            mail_not_sent = "Mail not sent to user"
        data['mail_not_sent'] = mail_not_sent
        return Response(data, status=status.HTTP_201_CREATED)
    else:
        return Response({"message": serialized._errors, "token": {}, "code": 600})
        # return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)


class Login(APIView):
    permission_classes = (AllowAny,)

    @swagger_auto_schema(request_body=UserLoginSerializer(many=True),
                         responses={200: 'Email Exist', 600: 'requested data invalid',
                                    202: 'verification email sent for first time user',
                                    2003: 'User is not verified, mail sent to user for otp verification',
                                    2004: 'Password not set, Ask user to set password.',
                                    5001: 'Invalid email', 5002: 'exception'})
    def post(self, request):
        try:
            email = request.data.get('email', None)
            if email:
                user_obj = User.objects.filter(email__iexact=email)
                if user_obj.exists():
                    token, created = Token.objects.get_or_create(user=user_obj.first())
                    otp_obj = OtpToVerify.objects.get(user=user_obj.first())
                    if otp_obj.is_verify:
                        if user_obj.first().password_custom is not None:
                            return Response({"message": "Email Exist", "token": token.key, "code": 200})
                        else:
                            return Response({"message": "Password not set.", "token": token.key, "code": 2004})
                    else:
                        subject = 'Verify Email for Apni Rasoi Registration'
                        message = 'Hope you are enjoying.\n\n ' + str(otp_obj.otp) + ' is your OTP for verification.'
                        recepient = str(user_obj.first().email)
                        send_mail(subject, message, EMAIL_HOST_USER, [recepient], fail_silently=False)
                        return Response({"message": "User is not verified, mail sent to user for otp verification",
                                         "token": token.key, "code": 2003})
                else:
                    serialized = UserSerializer(data=request.data)
                    data = {}
                    if serialized.is_valid():
                        user = User.objects.create_user(
                            email=serialized.validated_data['email'],
                            date_joined=datetime.datetime.now(),
                        )
                        if user:
                            token = Token.objects.create(user=user)
                            json = serialized.data
                            json['token'] = token.key
                            data['data'] = serialized.data
                            data['token'] = token.key
                    else:
                        return Response({"message": serialized._errors, "token": {}, "code": 600})
                    otp_obj = OtpToVerify.objects.create(otp=random.randrange(500000, 1000000, 6), user=user)
                    subject = 'Verify Email for Apni Rasoi Registration'
                    message = 'Hope you are enjoying.\n\n '+ str(otp_obj.otp) +' is your OTP for verification.'
                    recepient = str(user.email)
                    send_mail(subject, message, EMAIL_HOST_USER, [recepient], fail_silently=False)
                    return Response({"message": "Email sent for verification", "token": token.key, "code": 202})
            else:
                message = "Invalid email"
                return Response({"message": message, "code": 5001, 'token': {}})
        except Exception as e:
            return Response({"message": str(e), "code": 5000, 'token': {}})


class MyProfile(viewsets.ViewSet):
    permission_classes = [ReadOnly]

    @swagger_auto_schema(responses={200: UserProfileSerializer(many=True), 5010: 'error'})
    def profile(self, request):
        try:
            user = Token.objects.filter(key=request.headers['token']).first()
            serializer = UserProfileSerializer(user.user, context={"request": request})
            return Response(serializer.data)
        except Exception as e:
            return Response({"message": str(e), "code": 5010, 'token': {}})


class Logout(APIView):
    permission_classes = (AllowAny,)

    @swagger_auto_schema(request_body=LogOutSerializer(many=True), responses={7000: 'Successfully logged out.', 7001: 'error'})
    def post(self, request):
        return self.logout(request)

    def logout(self, request):
        try:
            user = Token.objects.filter(key=request.headers['token']).first()
            user.delete()
            return Response({"success": ("Successfully logged out."), 'status': 7000}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"error": str(e), 'status': 7001})


class OTPVerification(APIView):
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=UserOTPSerializer(many=True),
                         responses={3000: 'Otp Verified.', 3001: 'error',
                                    3002: 'user is already verified', 3003: 'otp not matched'})
    def post(self, request):
        user = Token.objects.filter(key=request.headers['token']).first()
        try:
            otp_obj = OtpToVerify.objects.get(user=user.user, otp=request.data['otp'])
            if otp_obj:
                if otp_obj.is_verify:
                    return Response({'message': 'User is already verified', 'status': 3002})
                otp_obj.is_verify = True
                # otp_obj.user.is_active = True
                otp_obj.save()
                subject = 'Welcome to Apni Rasoi Services'
                message = 'You are successfully registered'
                recepient = str(user.user)
                send_mail(subject, message, EMAIL_HOST_USER, [recepient], fail_silently=False)
                return Response({'message': 'otp is verified', 'status': 3000})
            else:
                return Response({'message': 'otp not matched', 'status': 3003})
        except Exception as e:
            return Response({'message': str(e), 'status': 3001})


class SetPassword(APIView):
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=SetPasswordSerializer(many=True),
                         responses={4000: 'password updated', 4002: 'user not exist', 4003: 'error'})
    def post(self, request):
        user = Token.objects.filter(key=request.headers['token']).first()
        try:
            user_obj = User.objects.get(email=user.user)
            if user_obj:
                user_obj.set_password(request.data['password'])
                user_obj.password_custom = request.data['password']
                user_obj.save()
                return Response({'message': 'password updated', 'status': 4000})
            else:
                return Response({'message': 'user not exist', 'status': 4002})
        except Exception as e:
            return Response({'message': str(e), 'status': 4001})


class EmailPassword(APIView):
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=UserPasswordSerializer(many=True),
                         responses={8000: 'Password Match', 401: 'Login Failed',
                                    8002: 'Password not matched', 8001: 'exception'})
    def post(self, request):
        try:
            user = Token.objects.filter(key=request.headers['token']).first()
            user_obj = User.objects.get(email=user.user)
            password = request.data['password']
            if user.user and user_obj.check_password(password):
                user = authenticate(username=user.user, password=password)
                if not user:
                    return Response({"error": "Login failed"}, status=HTTP_401_UNAUTHORIZED)
                return Response({"message": "Password Matched", "token": request.headers['token'], "code": 8000})
            else:
                message = "Password not matched"
                return Response({"message": message, "code": 8002, 'token': {}})
        except Exception as e:
            return Response({'message': str(e), 'status': 8001})


class UserAddr(APIView):
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=UserAddressSerializer(many=True),
                         responses={9000: 'Address saved',
                                    9001: 'Something went wrong',
                                    9002: 'parameter missing'})
    def post(self, request):
        try:
            user = Token.objects.filter(key=request.headers['token']).first()
            serialized = UserAddressSerializer(data=request.data)
            if serialized.is_valid():
                user_obj = User.objects.get(email=user.user)
                user_obj.addr = request.data.get('address', None)
                user_obj.postalCode = request.data.get('postal_code', None)
                user_obj.contactNo = request.data.get('contact', None)
                user_obj.save()
                return Response({"message": "Address saved", "token": request.headers['token'], "code": 9000})
            else:
                return Response({"message": serialized._errors, "token": {}, "code": 9002})
        except Exception as e:
            return Response({'message': str(e), 'status': 9001})