from django.conf.urls import url
from django.urls import path
from user_listing.views import Login, MyProfile, Logout, OTPVerification, SetPassword, EmailPassword, UserAddr


urlpatterns = [
    # ...

    # Some where in your existing urlpatterns list, Add this line
    path('user-email-check', Login.as_view(), name="user-email-check"),
    path('user-email-password', EmailPassword.as_view(), name="user-email-password"),
    path('user-address', UserAddr.as_view(), name="user-address"),
    path('my-profile', MyProfile.as_view({'get': 'profile'}), name="user-profile"),
    path('logout', Logout.as_view()),
    path('otp-verification', OTPVerification.as_view()),
    path('set-password', SetPassword.as_view()),

    # ...
]
from rest_framework.authtoken import views
urlpatterns += [
    # url(r'^api-token-auth/', views.obtain_auth_token)
]