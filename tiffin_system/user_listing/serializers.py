from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from user_listing.models import User, OtpToVerify


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        # Tuple of serialized model fields (see link [2])
        fields = ("email",)

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.save()
        return user


class ProfileSerializer(ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'email', 'is_active', 'date_joined', )


class UserLoginSerializer(ModelSerializer):

    class Meta:
        model = User
        fields = ('email', )


class UserProfileSerializer(ModelSerializer):
    user_image = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('email', 'username', 'first_name', 'last_name', 'addr', 'postalCode',
                  'contactNo', 'is_active', 'user_image')

    def get_user_image(self, user):
        request = self.context.get('request')
        if user.user_img and hasattr(user.user_img, 'url'):
            return request.build_absolute_uri(user.user_img.url)
        return None


class LogOutSerializer(serializers.Serializer):
    token = serializers.CharField()


class SetPasswordSerializer(serializers.Serializer):
    token = serializers.CharField()
    password = serializers.CharField()


class UserOTPSerializer(serializers.Serializer):
    token = serializers.CharField()
    otp = serializers.IntegerField()


class UserPasswordSerializer(serializers.Serializer):
    token = serializers.CharField()
    password = serializers.CharField()


class UserAddressSerializer(serializers.Serializer):
    address = serializers.CharField()
    postal_code = serializers.IntegerField()
    contact = serializers.IntegerField()
