from django.contrib import admin

# Register your models here.
from user_listing.models import User, OtpToVerify


class OtpToVerifyInline(admin.TabularInline):
    model = OtpToVerify
    extra = 0
    fieldsets = (
            (None, {
                'fields': ('otp', 'is_verify')
            }),
    )
    readonly_fields = ('otp',)


@admin.register(User)
class UserInstanceAdmin(admin.ModelAdmin):

    fieldsets = (
        (None, {
            'fields': ('email', 'first_name', 'last_name')
        }),
        ('Contact Details', {
            'fields': ('contactNo', 'addr', 'postalCode')
        }),
        ('User Image', {
            'fields': ('user_img', 'usr_image_tag',)
        }),
        ('Availability', {
            'fields': ('is_active',)
        }),
    )
    # fields = ['image_tag']
    readonly_fields = ['usr_image_tag', 'email', 'user_img', 'contactNo']
    inlines = [OtpToVerifyInline]
