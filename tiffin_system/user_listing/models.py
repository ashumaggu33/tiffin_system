from __future__ import unicode_literals
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.core.mail import send_mail
from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.db.models import DO_NOTHING
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.safestring import mark_safe
from phone_field import PhoneField


# class Profile(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, editable=False)
#     bio = models.TextField(max_length=500, blank=True, verbose_name="Biodata")
#     location = models.CharField(max_length=30, blank=True, verbose_name="Location")
#     birth_date = models.DateField(null=True, blank=True, verbose_name="Birth Date")
#     contact_number = PhoneField(max_length=30, blank=True, verbose_name="Contact Number", null=True)
#
#     class Meta:
#         verbose_name = 'User Profile'
#         verbose_name_plural = 'User Profile Details'
#
#     def __str__(self):
#         return self.user.email
#
#
# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         Profile.objects.create(user=instance)
#
#
# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, **kwargs):
#     instance.profile.save()


class UserManager(BaseUserManager):

    def normalize_email(cls, email):
        """
        Normalize the email address by lowercasing the domain part of it.
        """
        email = email or ''
        try:
            email_name, domain_part = email.strip().rsplit('@', 1)
        except ValueError:
            pass
        else:
            email = email_name + '@' + domain_part.lower()
        return email

    def create_user(self, email, date_joined, password=None, addr=None, profile_picture=None):
        if not email:
            raise ValueError("User must have an email")
        user = self.model(
            email=self.normalize_email(email)
        )
        user.username = email
        user.set_password(password)  # change password to hash
        user.user_img = profile_picture
        user.is_admin = False
        user.is_staff = False
        user.is_active = True
        user.date_joined = date_joined
        user.addr = addr
        user.save()
        return user

    def create_superuser(self, email, password=None, is_active=True, **extra_fields):
        if not email:
            raise ValueError("User must have an email")
        if not password:
            raise ValueError("User must have a password")

        user = self.model(
            email=self.normalize_email(email)
        )
        user.username = email
        if password is not None:
            user.set_password(password)
        user.user_img = None
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.is_active = is_active
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(verbose_name='email address', unique=True)
    username = models.EmailField(verbose_name='username', unique=True, blank=True)
    first_name = models.CharField(verbose_name='first name', max_length=30, blank=True)
    last_name = models.CharField(verbose_name='last name', max_length=30, blank=True)
    addr = models.TextField(verbose_name='address', blank=True, null=True)
    postalCode = models.IntegerField(verbose_name='Postal Code',  null=True, blank=True)
    contactNo = models.IntegerField(verbose_name='Contact',  null=True, blank=True)
    date_joined = models.DateTimeField(verbose_name='date joined', auto_now_add=True)
    is_active = models.BooleanField(verbose_name='active', default=True)
    is_admin = models.BooleanField(verbose_name='admin', default=True)
    is_superuser = models.BooleanField(verbose_name='superuser', default=True)
    is_staff = models.BooleanField(verbose_name='staff', default=True)
    user_img = models.ImageField(upload_to='user_images/', null=True, blank=True, default='')
    password_custom = models.CharField(max_length=100, default=None, null=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def usr_image_tag(self):
        return mark_safe('<img src="%s" width="150" height="150" />' % str(self.user_img.url))
    usr_image_tag.short_description = 'Image'
    usr_image_tag.allow_tags = True

    def usr_image_url(self):
        return self.user_img.url


class OtpToVerify(models.Model):

    otp = models.IntegerField(verbose_name='OTP')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_verify = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'OTP to verify user'
        verbose_name_plural = 'OTP to verify user'

    def __str__(self):
        return str(self.otp)
